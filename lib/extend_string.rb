# encoding: utf-8

class String
  DIACRITICS_SEARCH = [:ç, :æ, :œ, :á, :é, :ě, :í, :ó, :ú, :a, :e, :i, :o, :u, :ä, :ë, :i, :ö, :ü, :y, :ý, :â, :ŕ, :ř, :č, :ď, :e, :î, :ô, :u, :a, :e, :i, :o, :u, :č, :š, :ž, :ň, :ń, :š, :ň, :ú, :ť, :ľ, :ĺ, :Á, :É, :Í, :Ó, :Ú, :Ŕ, :Č, :Ć, :Ď, :Ĺ, :Ľ, :Ň, :Ń, :Š, :Ś, :Ť, :Ř, :Ž, :Ź, :Ý, :Ö, :Ä, :Ů, :Ü, :Ô, :Ě, :ő, :Ą, :ű]
  DIACRITICS_REPLACE = [:c, :ae, :oe, :a, :e, :e, :i, :o, :u, :a, :e, :i, :o, :u, :a, :e, :i, :o, :u, :y, :y, :a, :r, :r, :c, :d, :e, :i, :o, :u, :a, :e, :i, :o, :u, :c, :s, :z, :n, :n, :s, :n, :u, :t, :l, :l, :A, :E, :I, :O, :U, :R, :C, :C, :D, :L, :L, :N, :N, :S, :S, :T, :R, :Z, :Z, :Y, :O, :A, :U, :U, :O, :E, :o, :a, :u]

  UPCASE_DIACRITIC = ['Á', 'É', 'Í', 'Ó', 'Ú', 'Ŕ', 'Č', 'Ď', 'Ĺ', 'Ľ', 'Ň', 'Š', 'Ť', 'Ř', 'Ž', 'Ý']
  DOWNCASE_DIACRITIC = ['á', 'é', 'í', 'ó', 'ú', 'ŕ', 'č', 'ď', 'ĺ', 'ľ', 'ň', 'š', 'ť', 'ř', 'ž', 'ý']

  def remove_accents
    remove_diacritics self
  end

  def count_accents
    str = self
    count = 0
    str.each do |ch|
      count+=1 if DIACRITICS_SEARCH.include? ch
    end
    count
  end

  def friendly_url
    str = String.new(self)
    str = remove_accents
    ["+", "?", " ", ",", ".", "'", "/", "\"", "&", "]","["].each do |replace|
      str = str.gsub(replace, "-")
    end
    str = str.gsub("----", "-").gsub("---", "-").gsub("--", "-")
    str = str.downcase_my
    str = str[1..-2] if str.start_with? "-"
    str = str[0..-2] if str.end_with? "-"
    str
  end

  def remove_special_chars
    str = String.new(self)
    ["+", "?", " ", ",", ".", "'", "/", "\"", "&", "]","["].each do |replace|
      str = str.gsub(replace, " ")
    end
    str = str.gsub("    ", " ").gsub("   ", " ").gsub("  ", " ")
    str.strip
  end

  def gsub_from_array(hash)
    str = self
    hash.each_pair do |key, value|
      str = str.gsub(key, value)
    end
    str
  end

  def downcase_my
    str = self.downcase
    UPCASE_DIACRITIC.each_with_index do |search, i|
      replace = DOWNCASE_DIACRITIC[i]
      str = str.gsub(search.to_s, replace.to_s)
    end
    str
  end

  def upcase_my
    str = self.upcase
    DOWNCASE_DIACRITIC.each_with_index do |search, i|
      replace = UPCASE_DIACRITIC[i]
      str = str.gsub(search.to_s, replace.to_s)
    end
    str
  end


  def only_numbers?
    !self.match(/^\d+$/).nil?
  end

  def clean_blank
    str = self
    str = str.gsub("''", "")
    return nil if str.blank?
    str
  end

  def eql_with?(str2, ratio=1.0)
    str1 = change_chars self.strip, [:downcase, :diacritic, :space, :stop]
    str2 = change_chars str2.strip, [:downcase, :diacritic, :space, :stop]
    return true if str1.eql? str2
    n_gram_ratio(str1, str2) >= ratio
  end

  def n_gram_ratio(str1, str2, n=3)
    str1 = change_chars(str1, [:downcase, :diacritic, :space, :stop])
    str2 = change_chars(str2, [:downcase, :diacritic, :space, :stop])
    n_gram = NGram.new([str1, str2], :n => n)
    union = n_gram.ngrams_of_all_data[n].size
    intersect = 0
    n_gram.ngrams_of_all_data[n].each do |item|
      intersect+= 1 if item.second == 2
    end
    jaccard_ratio(intersect, union)
  end

  def jaccard_ratio(intersect, union)
    return 0.0 if union.eql? 0
    intersect.to_f / union.to_f
  end

  def change_chars(str, options = {})
    return "" if str.nil?
    options.each do |option|
      str = str.downcase if option.eql? :downcase
      str = remove_diacritics(str) if option.eql? :diacritic
      str = remove_space(str) if option.eql? :space
      str = remove_stop_char(str) if option.eql? :stop
    end
    str
  end

  def remove_space(str)
    str.gsub(/\s+/, "")
  end

  def remove_stop_char(str)
    str.gsub(/[^0-9a-z]/i, '')
  end

  def remove_diacritics(str)
    DIACRITICS_SEARCH.each_with_index do |search, i|
      replace = DIACRITICS_REPLACE[i]
      str = str.gsub(search.to_s, replace.to_s)
    end
    str
  end

  def remove_ascii(remove_ascii)
    str = ""
    self.each_char do |char|
      str+= char unless remove_ascii.include? char.ord
    end
    str
  end

  def format_number
    str = self
    if str.include? "."
      parts = str.split(".")
    elsif str.include? ","
      parts = str.split(",")
    else
      parts = [str, nil]
    end
    parts[0] = parts[0].reverse.gsub(/...(?=.)/, '\& ').reverse
    return parts[0] if parts[1].nil?
    parts[0] + "," + parts[1]
  end
end