namespace :parsing_and_extracting do

  task :parse_and_extract => :environment do
    start_nth = ENV['START_NTH'] || nil
    start_nth = (start_nth) ? (start_nth.to_i-1) : nil
    start_file = ENV['START_WITH'] || nil

    end_nth = ENV['END_NTH'] || nil
    end_nth = (end_nth) ? (end_nth.to_i-1) : nil
    end_file = ENV['END_WITH'] || nil

    parsing_and_extracting_service = ParsingAndExtracting::Service.new
    # start with nth
    start_file = parsing_and_extracting_service.find_nth_file(start_nth) if start_nth && !start_file
    end_file = parsing_and_extracting_service.find_nth_file(end_nth) if end_nth && !end_file

    parsing_and_extracting_service.parse_and_extract(start_file, end_file)
  end

  task :find_nth_file => :environment do
    nth = ENV['NTH'] || nil
    nth = (nth) ? (nth.to_i-1) : nil
    parsing_and_extracting_service = ParsingAndExtracting::Service.new
    puts parsing_and_extracting_service.find_nth_file(nth)
  end

  task :view_file => :environment do
    nth = ENV['NTH'] || nil
    nth = (nth) ? (nth.to_i-1) : nil
    parsing_and_extracting_service = ParsingAndExtracting::Service.new
    filename = parsing_and_extracting_service.find_nth_file(nth)
    parsing_and_extracting_service.view_file(filename)
  end

  task :create_authors_fulltext_index => :environment do
    parsing_and_extracting_service = ParsingAndExtracting::Service.new
    parsing_and_extracting_service.create_authors_fulltext_index
  end
end
