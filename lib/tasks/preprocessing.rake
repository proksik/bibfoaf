namespace :preprocessing do

  task :clean => :environment do
    preprocessing_service = Preprocessing::Service.new
    preprocessing_service.clean

  end

  task :split => :environment do
    preprocessing_service = Preprocessing::Service.new
    preprocessing_service.split
  end

  task :analyze => :environment do
    preprocessing_service = Preprocessing::Service.new
    preprocessing_service.analyze
  end

end
