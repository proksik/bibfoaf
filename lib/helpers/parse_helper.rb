require 'extend_string'

module Helpers
  module ParseHelper

    def transform_to_index(title)
      return nil unless title
      title.remove_accents.remove_special_chars
    end

    def parse_title(title)
      return nil unless title
      unless title.is_a? String
        title = title.map { |_, v| v }.join('')
      end
      parse_str(title)
    end

    def parse_series(item)
      return nil unless item
      unless item.is_a? String
        item = item.map { |_, v| v }.join('')
      end
      parse_str(item)
    end

    def parse_publisher(item)
      return nil unless item
      unless item.is_a? String
        item = item.map { |_, v| v }.join('')
      end
      parse_str(item)
    end

    def parse_str(str)
      return nil unless str
      str = str[0..-2] if str.end_with? "."
      str
    end

    def parse_item(items)
      return nil unless items
      items.first
    end

    def parse_year(items)
      item = parse_item(items)
      return nil unless item
      item.to_i
    end
  end
end