# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BibFoaf::Application.config.secret_key_base = 'd883fa6723619c3f294b7ccf56418579c8728aaa91373c2dc9b0bc3c9b07c55f386aa8b045b8cda868cde937d0746010300bb39a2f6aa1ff44cb69355ca46bbc'
