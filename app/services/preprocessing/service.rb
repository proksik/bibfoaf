# encoding: utf-8

require 'xmlsimple'

module Preprocessing
  class Service

    CHARACTERS = ["&Agrave;", "À"], ["&Aacute;", "Á"], ["&Acirc;", "Â"], ["&Atilde;", "Ã"], ["&yuml;", "ÿ"],
        ["&Auml;", "Ä"], ["&Aring;", "Å"], ["&AElig;", "Æ"], ["&Ccedil;", "Ç"], ["&Egrave;", "È"],
        ["&Eacute;", "É"], ["&Ecirc;", "Ê"], ["&Euml;", "Ë"], ["&Igrave;", "Ì"], ["&Iacute;", "Í"],
        ["&Icirc;", "Î"], ["&Iuml;", "Ï"], ["&ETH;", "Ð"], ["&Ntilde;", "Ñ"], ["&Ograve;", "Ò"],
        ["&Oacute;", "Ó"], ["&Ocirc;", "Ô"], ["&Otilde;", "Õ"], ["&Ouml;", "Ö"], ["&Oslash;", "Ø"],
        ["&Ugrave;", "Ù"], ["&Uacute;", "Ú"], ["&Ucirc;", "Û"], ["&Uuml;", "Ü"], ["&Yacute;", "Ý"],
        ["&THORN;", "Þ"], ["&szlig;", "ß"], ["&agrave;", "à"], ["&aacute;", "á"], ["&acirc;", "á"],
        ["&atilde;", "ã"], ["&auml;", "ä"], ["&aring;", "å"], ["&aelig;", "æ"], ["&ccedil;", "ç"],
        ["&egrave;", "è"], ["&eacute;", "é"], ["&ecirc;", "ê"], ["&euml;", "ë"], ["&igrave;", "ì"],
        ["&iacute;", "í"], ["&icirc;", "î"], ["&iuml;", "ï"], ["&eth;", "ð"], ["&ntilde;", "ñ"],
        ["&ograve;", "ò"], ["&oacute;", "ó"], ["&ocirc;", "ô"], ["&otilde;", "õ"], ["&ouml;", "ö"],
        ["&oslash;", "ø"], ["&ugrave;", "ù"], ["&uacute;", "ú"], ["&ucirc;", "û"], ["&uuml;", "ü"],
        ["&yacute;", "ý"], ["&thorn;", "þ"]

    def substitute(string)
      CHARACTERS.each { |key, value| string.gsub! key, value } if string.include? ";"
      string
    end

    def clean
      File.delete(data_path("processed")) if File.exist?(data_path("processed"))
      File.open(data_path("processed"), "w") do |processed_file|
        File.open(data_path, "r").each do |line|
          new_line = substitute(line)
          processed_file.puts(new_line)
        end
      end
    end

    # split big XML to small XMLS

    def split
      f = File.open data_path("processed")

      ends = []
      f_idx = 0
      line_idx = 0
      g = File.open(data_path(f_idx), 'w')
      start_xml(g)

      # ignore first lines
      f.readline
      f.readline
      f.readline

      f.each_line do |line|
        ends << line if is_end?(line) && !ends.include?(line)
        g.write line if line != "</dblp>\n"
        line_idx+= 1
        if (line_idx > MAX_PER_FILE) && is_end?(line)
          end_xml(g)
          g.close
          g = File.open(data_path(f_idx), 'w')
          start_xml(g)
          f_idx+= 1
          line_idx = 0
        end
      end
      end_xml(g)
      g.close
      f.close
    end

    @structure = {}
    @counts = {}
    @index = {}

    # analyze XML - structure, counts, ...
    def analyze
      @structure = {}
      Dir.glob(data_path("*")) do |filename|
        puts "Process: #{filename}"
        parse_file filename
      end
      puts 'Structure:'
      @structure.each_pair do |key, keys|
        puts "#{key} - #{@counts[key]} items - #{keys.inspect}"
      end
    end

    private

    def analyze_file(filename)
      xml_data = File.open(filename)

      data = XmlSimple.xml_in(xml_data)

      data.each_pair do |key, values|
        unless @structure.include? key
          @structure[key] = values.first.keys
          @counts[key] = 0
          @index[key] = []
        end
        @counts[key] += values.count
      end
    end

    DATA_PATH = 'data/'

    def data_path(file_number = nil)
      if file_number
        filename = "_" + file_number.to_s
      else
        filename = ""
      end
      "#{DATA_PATH}dblp#{filename}.xml"
    end

    MAX_PER_FILE = 100000

    def is_end?(item)
      !item.scan(/^<\/\w*>$/).empty?
    end

    def start_xml(f)
      f.write "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE dblp SYSTEM \"dblp.dtd\">\n<dblp>\n"
    end

    def end_xml(f)
      f.write "</dblp>\n"
    end

  end
end
