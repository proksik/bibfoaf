require 'extend_string'

module Discovering
  class Service
    include Discovering::Includes::Common
    include Discovering::Includes::GroupAuthors


    def foaf_authors(author)
      neo = Neography::Rest.new
      q = neo.execute_query("START n=node(#{author.neo_id}) MATCH (n)--()--(f) RETURN f")
      authors = {}
      q['data'].each do |data|
        local_author_name = data.first['data']['name']
        if data.first['data']['type'] == 'author' && local_author_name != author.name
          authors[local_author_name] = 0 unless authors.has_key? local_author_name
          authors[local_author_name]+= 1
        end
      end
      Hash[authors.sort_by { |_, value| -value }]
    end


    def discover_group_authors(author)
      return [], nil unless author
      group_author(author)
    end

    def status
      res = {}
      res[:authors] = 1312883 # count_nodes('author')
      res[:journals] = 1349  #count_nodes('journal')
      res[:in_proceedings] = 1305426 #count_nodes('in_proceeding')
      res[:books] = 16485 #count_nodes('book')
      res[:articles] = 1019782# count_nodes('article')
      res[:years] = 79  #count_nodes('year')
      res[:all_nodes] = 3689269 #count_nodes
      res[:all_relationships] = 22400830 # count_relationships
      res[:all_properties] = 9089915 # count_relationships
      res[:all_relationship_types] = 8 # count_relationships
      #puts "books: #{count_nodes('book')}"
      #puts "journals: #{count_nodes('journal')}"
      #puts "in_proceedings: #{count_nodes('in_proceeding')}"
      #puts "in_collections: #{count_nodes('in_collection')}"
      #puts "years: #{count_nodes('year')}"
      #puts "total: #{count_nodes}"
      res
    end
  end
end