module Discovering
  module Includes
    module GroupAuthors

      def group_author(author)
        articles, articles_authors= memorize_author_entity_items(author, :articles)
        in_proceedings, in_proceedings_authors= memorize_author_entity_items(author, :in_proceedings)
        in_collections, in_collections_authors= memorize_author_entity_items(author, :in_collections)

        entities = [[articles_authors, :articles], [in_proceedings_authors, :in_proceedings], [in_collections_authors, :in_collections]]

        # [ {articles: ..., authors: ...}, {}, [] ]
        author_groups = [] # generate authors

        if ((articles.count + in_proceedings.count + in_collections.count) <= 5) &&
            years_in_interval(articles_authors, 5) && years_in_interval(in_proceedings_authors, 5) && years_in_interval(in_collections_authors, 5)
          author_groups << {authors: [], articles: [], in_proceedings: [], in_collections: []}

          entities.each do |item|
            entity_items = item[0]
            type = item[1]
            entity_items.each_pair do |entity_item_id, authors|
              author_groups[0][type] << entity_item_id
              author_groups[0][:authors] = (author_groups[0][:authors] + authors).uniq
            end
          end

        else
          entities.each do |item|
            entity_items = item[0]
            type = item[1]
            entity_items.each_pair do |entity_item_id, authors|
              dev_author_id = find_author(author_groups, authors, author)
              if dev_author_id
                author_groups[dev_author_id][type] << entity_item_id
                author_groups[dev_author_id][:authors] = (author_groups[dev_author_id][:authors] + authors).uniq
              else
                author_group = {authors: authors, articles: [], in_proceedings: [], in_collections: []}
                author_group[type] =[entity_item_id]
                author_groups << author_group
              end
            end
          end
          author_groups = join_groups(author_groups)
        end
        author_groups.map { |group_author|
          group_author[:articles].map! { |article_id| articles[article_id] }.sort!{|item_a, item_b| item_b.outgoing(:year).first[:year] <=> item_a.outgoing(:year).first[:year] }
          group_author[:in_proceedings].map! { |in_proc_id| in_proceedings[in_proc_id] }.sort!{|item_a, item_b| item_b.outgoing(:year).first[:year] <=> item_a.outgoing(:year).first[:year]}
          group_author[:in_collections].map! { |in_col_id| in_collections[in_col_id] }.sort!{|item_a, item_b| item_b.outgoing(:year).first[:year] <=> item_a.outgoing(:year).first[:year]}
          group_author
        }
      end

      private

      def memorize_author_entity_items(author, type)
        items = {}
        items_authors = {}
        author.outgoing(type).each do |item|
          authors = authors(item, author.neo_id.to_i)
          items_authors[item.neo_id.to_i] = authors
          items[item.neo_id.to_i] = item
        end
        return items, items_authors
      end

      def authors_intersect?(authors_a, authors_b, ignore_node = nil)
        return true if authors_a.empty? && authors_b.empty?
        return true unless (authors_a & authors_b).empty?
        if ignore_node
          authors_a.each do |author_a|
            authors_b.each do |author_b|
              return true if authors_rel_with_ignoring_node(author_a, author_b, ignore_node)
            end
          end
        end
        false
      end

      @colors = []

      def dfs_component(group_authors, visited_index, color)
        @colors[visited_index] = color
        visited_authors = group_authors[visited_index][:authors]
        group_authors.each_index do |index|
          dfs_component(group_authors, index, color) if @colors[index] == 0 && authors_intersect?(group_authors[index][:authors], visited_authors)
        end
      end

      def join_groups(group_authors)
        all_colors = 0
        @colors = Array.new(group_authors.size, all_colors)
        @author_groups = group_authors
        group_authors.each_index do |index|
          if @colors[index] == 0
            all_colors+= 1
            dfs_component(group_authors, index, all_colors)
          end
        end
        #puts @colors.inspect
        new_group_authors = []
        # all nodes has color
        (1..all_colors).each do |color|
          group_author = {articles: [], authors: [], in_proceedings: [], in_collections: []}
          group_authors.each_index do |index|
            if color == @colors[index]
              group_author[:authors] = (group_author[:authors] + group_authors[index][:authors]).uniq
              group_author[:articles] = group_author[:articles] + group_authors[index][:articles]
              group_author[:in_proceedings] = group_author[:in_proceedings] + group_authors[index][:in_proceedings]
              group_author[:in_collections] = group_author[:in_collections] + group_authors[index][:in_collections]
            end
          end
          new_group_authors << group_author
        end
        new_group_authors
      end

      def find_author(group_authors, authors, ignore_node = nil)
        group_authors.each_with_index do |group_author, index|
          return index if authors_intersect?(group_author[:authors], authors, (ignore_node) ? ignore_node.neo_id : nil)
        end
        nil
      end

    end
  end
end