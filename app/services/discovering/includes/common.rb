module Discovering
  module Includes
    module Common

      def authors_rel_with_ignoring_node(neo_id1, neo_id2, ignore_neo_id3=nil)
        @neo = Neography::Rest.new
        node1 = @neo.get_node(neo_id1)
        node2 = @neo.get_node(neo_id2)
        ignore_node = (ignore_neo_id3) ? @neo.get_node(ignore_neo_id3) : nil
        count = 0
        relationships = @neo.get_paths(node1, node2, [{type: 'articles', direction: 'out'},
                                                      {type: 'in_collections', direction: 'out'},
                                                      {type: 'in_proceedings', direction: "out"},
                                                      {type: 'authors', direction: 'out'},
                                                      {type: 'book', direction: 'out'}], 5, 'shortestPath').each do |rel|
          if !ignore_node || rel['nodes'].all? { |n| n != ignore_node['self'] }
            count+= 1
          end
        end
        # aspon 1 je v tej minimalnej ceste cez nejaky iny vrchol
        (count/(relationships.count*1.0)) > 0.1
      end

      def count_nodes(type=nil)
        @neo = Neography::Rest.new
        if type
          q = @neo.execute_query("START n=node:types_index(type='#{type}') RETURN COUNT(n)")
        else
          q = @neo.execute_query('START n=node(*) RETURN COUNT(n)')
        end
        q['data'][0][0].to_i
      end

      def count_relationships
        @neo = Neography::Rest.new
        q = @neo.execute_query('START n=relationship(*) RETURN COUNT(n)')
        q['data'][0][0].to_i
      end

      def authors(node, without_id = nil)
        node.outgoing(:authors).map { |author| author.neo_id.to_i }.select { |id| id != without_id }
      end

      def years_in_interval(item_ids, interval)
        min_year, max_year = -1, -1
        item_ids.each do |item_id|
          year = Neography::Node.load(item_id).outgoing(:year).first[:year]
          min_year = year if min_year == -1 || min_year > year
          max_year = year if max_year == -1 || max_year < year
        end
        (max_year-min_year) <= interval
      end


    end
  end
end