module AuthorsHelper

  def neo4j_node_url(neo4j_id)
    "http://localhost:7474/db/data/node/#{neo4j_id}"
  end

end
