class StatusController < ApplicationController
  def index
    discovering_service = Discovering::Service.new
    @status = discovering_service.status
  end
end
