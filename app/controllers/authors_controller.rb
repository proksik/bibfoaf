class AuthorsController < ApplicationController
  before_filter :save_query, only: :index

  def index
    if params[:name]
      discovering_service = Discovering::Service.new
      @authors = Author.fulltext_find(params[:name])
      if @authors.nil?
        @authors = []
      elsif @authors.is_a? Array
        @author = nil
        @author_groups = []
        @foaf_authors = []
      else
        @author = @authors
        @authors = [@author]
        @author_groups = discovering_service.discover_group_authors(@author)
        @foaf_authors = discovering_service.foaf_authors(@author)
      end
    end
  end

  def show
    discovering_service = Discovering::Service.new
    @author = Neography::Node.load(params[:id])
    @author_groups = discovering_service.discover_group_authors(@author)
    @foaf_authors = discovering_service.foaf_authors(@author)
    render 'index'
  end

  private

  def save_query
    session[:queries] = "" unless session.has_key?(:queries)
    if params[:name]
      session[:queries] += params[:name] + '|' if session[:queries].split('|').all? { |s| s != params[:name] }
    end
  end
end
