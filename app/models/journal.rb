class Journal < Neography::Node
  extend Helpers::ParseHelper

  def self.create(properties)
    Neography::Node.create({type: :journal}.merge(properties))
  end

  def self.find_or_create(title)
    return nil unless title
    journal =  Neography::Node.find('journals_index', :title, transform_to_index(title))
    unless journal
      journal = create(title: title)
      journal.add_to_index('journals_index', :title, transform_to_index(title))
      journal.add_to_index('types_index', :type, :journal)
    end
    journal
  end
end