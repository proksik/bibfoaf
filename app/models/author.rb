class Author < Neography::Node
  extend Helpers::ParseHelper

  def self.fulltext_index_name
    'authors_fulltext_index'
  end

  def self.url
    puts self.inspect
    self["self"]
  end

  def self.find(author_name)
    Neography::Node.find('authors_index', :name, transform_to_index(author_name))
  end

  def self.create(properties)
    Neography::Node.create({type: :author}.merge(properties))
  end

  def self.find_or_create(name)
    author = find(name)
    unless author
      author = create(name: name)
      author.add_to_index('authors_index', :name, transform_to_index(name))
      author.add_to_index('types_index', :type, :author)
    end
    author
  end

  def self.add_fulltext_index(author)
    author.add_to_index(fulltext_index_name, :name, transform_to_index(author[:name]))
  end

  def self.fulltext_find(term)
    term = transform_to_index(term)
    author = find(term)
    return author if author
    return [] if term.size < 3
    term = term.gsub(" ","")
    authors = Neography::Node.find(fulltext_index_name, "name:*#{term}*")
    authors
  end
end