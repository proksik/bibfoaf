# inproceedings - 1311826 items - ["mdate", "key", "author", "title", "pages", "year", "crossref", "booktitle", "url", "ee"]

class InProceeding < Neography::Node
  extend Helpers::ParseHelper

  def self.create(properties)
    Neography::Node.create({type: :in_proceeding}.merge(properties))
  end

  def self.extract(item)
    title = parse_title(parse_item(item['title']))

    in_proceeding = create(title: title, pages: parse_item(item['pages']), crossref: parse_item(item['crossref']), url: parse_item(item['url']), ee: parse_item(item['ee']))
    in_proceeding.add_to_index('types_index', :type, :in_proceeding)

    # author
    if item['author']
      item['author'].each do |author_name|
        author = Author.find_or_create(author_name)
        author.outgoing(:in_proceedings) << in_proceeding
        in_proceeding.outgoing(:authors) << author
      end
    end
    year = Year.find_or_create(parse_year(item['year']))
    #year
    if year
      in_proceeding.outgoing(:year) << year
      year.outgoing(:in_proceedings) << in_proceeding
    end

    # book
    book = Book.find_or_create(parse_str(parse_item(item['booktitle'])))
    if book
      in_proceeding.outgoing(:book) << book
      book.outgoing(:in_proceedings) << in_proceeding
    end
  end

  def self.year(node)
    node.outgoing(:year).first[:year]
  end

end