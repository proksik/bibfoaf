#book - 10041 items - ["mdate", "key", "editor", "title", "year", "publisher", "volume", "booktitle", "isbn", "ee", "series", "url"]

class Book < Neography::Node
  extend Helpers::ParseHelper

  def self.create(properties)
    Neography::Node.create({type: :book}.merge(properties))
  end

  def self.find_or_create(title)
    return nil unless title
    book = Neography::Node.find('books_index', :title, transform_to_index(title))
    unless book
      book = create(title: title)
      book.add_to_index('books_index', :title, transform_to_index(title))
      book.add_to_index('types_index', :type, :book)
    end
    book
  end

  def self.extract(item)
    title = parse_title(parse_item(item['title']))

    book = find_or_create(title)
    book[:editor] = parse_str(parse_item(item['editor']))
    book[:publisher] = parse_publisher(parse_item(item['publisher']))
    book[:volume] = parse_str(parse_item(item['volume']))
    book[:isbn] = parse_str(parse_item(item['isbn']))
    book[:ee] = parse_str(parse_item(item['ee']))
    book[:series] = parse_series(parse_item(item['series']))
    book[:url] = parse_str(parse_item(item['url']))
    book.save

    #year
    year = Year.find_or_create(parse_year(item['year']))
    if year
      book.outgoing(:year) << year
      year.outgoing(:books) << book
    end
  end

  def self.year(node)
    node.outgoing(:year).first[:year]
  end
end