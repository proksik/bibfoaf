# article - 1026012 items - ["mdate", "key", "author", "title", "pages", "year", "volume", "journal", "number", "ee", "url"]

class Article < Neography::Node
  extend Helpers::ParseHelper

  def self.create(properties)
    Neography::Node.create({type: :article}.merge(properties))
  end

  def self.extract(item)
    title = parse_title(parse_item(item['title']))

    article = create(title: title, pages: parse_item(item['pages']), volume: parse_item(item['volume']), number: parse_item(item['number']), url: parse_item(item['url']), ee: parse_item(item['ee']))
    article.add_to_index('types_index', :type, :article)

    # author
    if item['author']
      item['author'].each do |author_name|
        author = Author.find_or_create(author_name)
        author.outgoing(:articles) << article
        article.outgoing(:authors) << author
      end
    end

    #year
    year = Year.find_or_create(parse_year(item['year']))
    if year
      article.outgoing(:year) << year
      year.outgoing(:articles) << article
    end

    # article
    journal = Journal.find_or_create(parse_str(parse_item(item['journal'])))
    if journal
      article.outgoing(:journal) << journal
      journal.outgoing(:articles) << article
    end
  end

  def self.year(node)
    node.outgoing(:year).first[:year]
  end
end